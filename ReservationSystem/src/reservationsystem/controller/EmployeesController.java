/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.controller;

import java.awt.GridLayout;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import reservationsystem.DatabaseAccess;
import reservationsystem.model.Employee;
import reservationsystem.model.Employee;

/**
 *
 * @author cstuser
 */
public class EmployeesController implements Initializable {
    private Connection connection;
    private Employee selectedEmployee;
    private boolean modificationsMade = false;
    
    ObservableList<Employee> listOfEmployees = FXCollections.observableArrayList();
    @FXML private TableView<Employee> employeeTable;
    @FXML private TableColumn<Employee, Integer> employeeId;
    @FXML private TableColumn<Employee, String> firstName;
    @FXML private TableColumn<Employee, String> accessLevel;

    private String[] employee_columns = {"employeeId", "firstName", "accessLevel"};
    
    @FXML
    private Button employeeTest;
    @FXML private Button delete;
    @FXML private Button add;
    
    @FXML private Button searchButton;
    @FXML private TextField searchBox;
    @FXML private TextField displayFirstName;
    @FXML private TextField displayAccessLevel;
    @FXML private Button saveEdit;
    
     @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            System.out.println("IN EMPLOYEE CONTROLLER");
            initializeView();
            
            
            employeeTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null)
            {
                saveEdit.setDisable(true);
                selectedEmployee = newSelection; 
                //System.out.println(selectedEmployee.toString());
                displayFirstName.setText(selectedEmployee.getFirstName());
                displayAccessLevel.setText(selectedEmployee.getAccessLevel());
              //  txtCustomerEmail.setText(selectedCustomer.getEmail());
               // customerDepo.setText("$" + String.valueOf(selectedCustomer.getDepositAmount()) + "0");
            }      
        });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
    
    private void initializeView() {
        try {
            String query = "select * from Employee";
            connection = DatabaseAccess.getConnection();
            ResultSet resultSet = connection.createStatement().executeQuery(query);
            retrieveEmployees(resultSet);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void employeeTestHandler(ActionEvent event) {
        System.out.println("EMPLOYEE HANDLER FOUND!!");
    }
    
    private void retrieveEmployees(ResultSet resultSet) {
        try{
            saveEdit.setDisable(true);
            listOfEmployees.clear();
            while(resultSet.next()) {
                Employee employee = new Employee();
                employee.setEmployeeId(resultSet.getInt(employee_columns[0]));
                employee.setFirstName(resultSet.getString(employee_columns[1]));     
                employee.setAccessLevel(resultSet.getString(employee_columns[2]));   
                listOfEmployees.add(employee);

             }
             //employeeId.setCellValueFactory(new PropertyValueFactory<>("employeeId"));
             firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));  
             accessLevel.setCellValueFactory(new PropertyValueFactory<>("accessLevel"));       
             employeeTable.setItems(listOfEmployees);
         }
         catch(Exception e) {
            e.printStackTrace();
         }
    }
    
    @FXML
    private void searchEmployees(ActionEvent event) {
        try {
            String userQuery = searchBox.getText();
            String search = "";
            if(!userQuery.trim().equals(""))
            {
                System.out.println("not null");
                search = " where firstName like '" + userQuery+"%' or accessLevel like '" + userQuery+"%'";
            }
            String query = "select * from Employee" + search;           
            ResultSet searchResultSet = connection.createStatement().executeQuery(query);
            retrieveEmployees(searchResultSet);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
      
    @FXML
    private void deleteEmployee(ActionEvent event) {
        JFrame frame = new JFrame();
        try {
            if(selectedEmployee != null) {                
                frame.setAlwaysOnTop(true);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                int answer = JOptionPane.showConfirmDialog(frame,
                "Delete employee entry ?\nItem ID: " + selectedEmployee.getEmployeeId()+ " Name: " + selectedEmployee.getFirstName());
                if(answer == 0) {
                    listOfEmployees.clear();
                    Connection connection = DatabaseAccess.getConnection();
                    String query = "delete from Employee where employeeId = " + selectedEmployee.getEmployeeId();
                    connection.createStatement().executeUpdate(query);
                    initializeView();
                }              
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
             frame.dispose();
        }
    }
    
    @FXML
    private void addEmployee(ActionEvent event) {
        JFrame frame = new JFrame();
         try {
            listOfEmployees.clear();
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setAlwaysOnTop(true);
            JTextField jFirstName = new JTextField(15);
            JTextField jAccessLevel = new JTextField(15);
           
            JPanel addEmployeePanel = new JPanel(new GridLayout(0,2)); 

            addEmployeePanel.add(new JLabel("Employee name: "));
            addEmployeePanel.add(jFirstName);
            addEmployeePanel.add(new JLabel("Access level: "));
            addEmployeePanel.add(jAccessLevel);

            int add = JOptionPane.showConfirmDialog(frame, addEmployeePanel, "New employee entry", JOptionPane.OK_CANCEL_OPTION);
            if(add == JOptionPane.OK_OPTION){               
                Connection connection = DatabaseAccess.getConnection();
               
                String name = jFirstName.getText();    
                String access = jAccessLevel.getText();
                
              connection.createStatement().executeUpdate("insert into Employee(firstName, accessLevel) values ('"+name+"', '" + access + "')");
                                                                     

            }
            initializeView();
        
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
             frame.dispose();
         }
    } 
    
    public void updateEmployee(ActionEvent event){
            listOfEmployees.clear();
            Connection connect = DatabaseAccess.getConnection();

            try {
                int update = connect.createStatement().executeUpdate("update Employee set firstName = '" 
                        + displayFirstName.getText() + "', accessLevel = '" + displayAccessLevel.getText()
                        + "' where employeeId = " + selectedEmployee.getEmployeeId());
            } catch (SQLException ex) {
                Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            initializeView();
    } 
    
    @FXML
    private void fieldModified() {
        if(selectedEmployee != null) {
            modificationsMade = true;
            saveEdit.setDisable(false);
        }
    }
}
