/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.controller;

import java.awt.GridLayout;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import reservationsystem.DatabaseAccess;
import reservationsystem.model.InventoryItem;

/**
 *
 * @author cstuser
 */
public class InventoryController implements Initializable {
   // private Connection connection;
    private ResultSet resultSet;
    private InventoryItem selectedItem;
    private boolean modificationsMade = false;
    //private PreparedStatement preparedStatement;
    
    ObservableList<InventoryItem> listOfProducts = FXCollections.observableArrayList();
    @FXML private TableView<InventoryItem> inventoryTable;
    @FXML private TableColumn<InventoryItem, Integer> productId;
    @FXML private TableColumn<InventoryItem, String> productName;
    @FXML private TableColumn<InventoryItem, String> productType;
    @FXML private TableColumn<InventoryItem, String> productSource;
    @FXML private TableColumn<InventoryItem, Double> productPrice;
    //@FXML private TableColumn<InventoryItem, String> productDateOfRelease;
    //@FXML private TableColumn<InventoryItem, String> productPublisherName;
    
    
    private String[] inventory_columns = {"productId", "productName", "type", "source", "price", "dateOfRelease", "publisherName"};


    @FXML private Button inventoryTest;
    @FXML private Button searchButton;
    @FXML private TextField searchBox;
    @FXML private Button delete;
    @FXML private Button add;
    
    @FXML private TextField displayId;
    @FXML private TextField displayProductName;
    @FXML private TextField displayType;
    @FXML private TextField displaySource;
    @FXML private TextArea displayPrice;
    @FXML private TextArea displayDateOfRelease;
    @FXML private TextField displayPublisherName;
    @FXML private Button saveEdit;
      
     @Override
    public void initialize(URL url, ResourceBundle rb) {
       try {
            System.out.println("IN INVENTORY CONTROLLER");
            initializeView();
            
           
           inventoryTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null)
            {
                saveEdit.setDisable(true);
                selectedItem = newSelection; 
               // System.out.println(selectedItem.toString());
                displayId.setText(Integer.toString(selectedItem.getProductId()));
                displayProductName.setText(selectedItem.getProductName());
                displayType.setText(selectedItem.getType());
                displaySource.setText(selectedItem.getSource());
                displayPrice.setText(Double.toString(selectedItem.getPrice()));               
                displayDateOfRelease.setText(selectedItem.getDateOfRelease());
                displayPublisherName.setText(selectedItem.getPublisherName());             
            }      
        });
       }
       catch(Exception e) {
           e.printStackTrace();
       }
    }    
    
    private void initializeView() {
        try {
        String query = "select * from Inventory";
           Connection connection = DatabaseAccess.getConnection();
           resultSet = connection.createStatement().executeQuery(query);
           retrieveInventory();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void inventoryTestHandler(ActionEvent event) {
        System.out.println("INVENTORY HANDLER FOUND!!");
    }
    
   
    private void retrieveInventory() {

        try {
           saveEdit.setDisable(true);
            listOfProducts.clear();
            while(resultSet.next()) {
                InventoryItem item = new InventoryItem();
                item.setProductId(resultSet.getInt(inventory_columns[0]));
                item.setProductName(resultSet.getString(inventory_columns[1]));
                item.setType(resultSet.getString(inventory_columns[2]));
                item.setSource(resultSet.getString(inventory_columns[3]));
                item.setPrice(resultSet.getDouble(inventory_columns[4]));
                item.setDateOfRelease(resultSet.getString(inventory_columns[5]));
                item.setPublisherName(resultSet.getString(inventory_columns[6]));
                listOfProducts.add(item);

             }
             productId.setCellValueFactory(new PropertyValueFactory<>("productId"));
             productName.setCellValueFactory(new PropertyValueFactory<>("productName"));
             productType.setCellValueFactory(new PropertyValueFactory<>("type"));
             productSource.setCellValueFactory(new PropertyValueFactory<>("source"));
             productPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
            // productDateOfRelease.setCellValueFactory(new PropertyValueFactory<>("dateOfRelease"));
            // productPublisherName.setCellValueFactory(new PropertyValueFactory<>("productPublisherName"));
             inventoryTable.setItems(listOfProducts);
         }
         catch(Exception e) {
            e.printStackTrace();
         }

    }
    
    @FXML
    private void searchInventory(ActionEvent event) {
        try {
            String userQuery = searchBox.getText();
            String search = "";
            if(!userQuery.trim().equals(""))
            {
                System.out.println("not null");
                search = " where productId like '" + userQuery+"%' or productName like '" + userQuery+"%' or type like '" + userQuery+"%' or source like '" + userQuery+"%' or price like '" + userQuery+"%' or dateOfRelease like '" + userQuery+"%' or publisherName like '" + userQuery + "%'";
                System.out.println(search);
            }
            String query = "select * from Inventory" + search;
            Connection connection = DatabaseAccess.getConnection();
            resultSet = connection.createStatement().executeQuery(query);
            retrieveInventory();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void deleteItem(ActionEvent event) {
        JFrame frame = new JFrame();
        try {
            if(selectedItem != null) {                
                frame.setAlwaysOnTop(true);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                int answer = JOptionPane.showConfirmDialog(frame,
                "Delete inventory item ?\nItem ID: " + selectedItem.getProductId() + " Name: " + selectedItem.getProductName());
                if(answer == 0) {
                    listOfProducts.clear();
                    Connection connection = DatabaseAccess.getConnection();
                    String query = "delete from Inventory where productId = " + selectedItem.getProductId();
                    connection.createStatement().executeUpdate(query);
                    initializeView();
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            frame.dispose();
        }
    }
    
    @FXML
    private void addItem(ActionEvent event) {
        JFrame frame = new JFrame();
        try {
            listOfProducts.clear();
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setAlwaysOnTop(true);
            JTextField jProductId = new JTextField(15);
            JTextField jProductName = new JTextField(15);
            JTextField jType = new JTextField(15);
            JTextField jSource = new JTextField(15);
            JTextField jPrice = new JTextField(8);
            JTextField jDateOfRelease = new JTextField(15);
            JTextField jPublisher = new JTextField(15);
           
            JPanel addItemPanel = new JPanel(new GridLayout(0,2)); 

            //addItemPanel.add(new JLabel("Prod name: "));
            //addItemPanel.add(jProductId);
            
            addItemPanel.add(new JLabel("Product name: "));
            addItemPanel.add(jProductName);
            
            addItemPanel.add(new JLabel("Type name: "));
            addItemPanel.add(jType);
            
            addItemPanel.add(new JLabel("Source: "));
            addItemPanel.add(jSource);
            
            addItemPanel.add(new JLabel("Price: "));
            addItemPanel.add(jPrice);
            
            addItemPanel.add(new JLabel("Date of release: "));
            addItemPanel.add(jDateOfRelease);
            
            addItemPanel.add(new JLabel("Publisher name: "));
            addItemPanel.add(jPublisher);
            

            int add = JOptionPane.showConfirmDialog(frame, addItemPanel, "New product entry", JOptionPane.OK_CANCEL_OPTION);
            if(add == JOptionPane.OK_OPTION){               
                Connection connection = DatabaseAccess.getConnection();
               
                String productName = jProductName.getText();               
                String type = jType.getText();
                String source = jSource.getText();
                Double price = Double.parseDouble(jPrice.getText());
                String release = jDateOfRelease.getText();
                String publisher = jPublisher.getText();
                
                //"productId", "productName", "type", "source", "price", "dateOfRelease", "publisherName"};
              connection.createStatement().executeUpdate("insert into Inventory(productName, type, source, price, dateOfRelease, publisherName) values ('"
                                                                 +productName+"', '"+type+"', '"+source+"', '"+price+"', '"+release+"', '"+ publisher+"')");
                                                                     

            }
            initializeView();
        
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            frame.dispose();
        }
    }
    
    public void updateItem(ActionEvent event){
            listOfProducts.clear();
            Connection connect = DatabaseAccess.getConnection(); 
            double deposit = 0;
            if(displayPrice.getText().equals(""))
                deposit = 0.0;

            else if(displayPrice.getText() != "")
                deposit = Double.parseDouble(displayPrice.getText());

            try {
                int update = connect.createStatement().executeUpdate("update Inventory set productName = '" + displayProductName.getText()
                        + "', type = '" + displayType.getText() + "', source = " + Integer.parseInt(displaySource.getText()) + ", price = "
                        + deposit + ", dateOfRelease = '" + displayDateOfRelease.getText() + "', publisherName = '" + displayPublisherName.getText()
                        + "' where productId = " + selectedItem.getProductId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            initializeView();
    }
    
    @FXML
    private void fieldModified() {
        if(selectedItem != null) {
            modificationsMade = true;
            saveEdit.setDisable(false);
        }
    }
}
