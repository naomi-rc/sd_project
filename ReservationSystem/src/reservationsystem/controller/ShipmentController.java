/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import reservationsystem.DatabaseAccess;
import reservationsystem.model.Shipment;
import reservationsystem.model.Shipment;

/**
 *
 * @author cstuser
 */
public class ShipmentController implements Initializable{
    private Connection connection;
    private Shipment selectedShipment;
    private boolean modificationsMade = false;
    
    ObservableList<Shipment> listOfShipments = FXCollections.observableArrayList();
    @FXML private TableView<Shipment> shipmentTable;
    @FXML private TableColumn<Shipment, Integer> shipmentId;
    @FXML private TableColumn<Shipment, String> dateOfReceipt;
    
    @FXML private Button shipmentTest;
    @FXML private Button saveEdit;
    @FXML private Button searchButton;
    @FXML private Button delete;
    @FXML private Button add;
    @FXML private TextField searchBox;
    
    @FXML private TextField displayShipmentId;
    @FXML private TextField displayShipmentDateOfReceipt;
    
    private String[] shipment_columns = {"shipmentId", "dateOfReceipt"};
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            System.out.println("IN SHIPMENT CONTROLLER");
            initializeView();
            
            shipmentTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null)
            {
                saveEdit.setDisable(true);
                selectedShipment = newSelection;                 
                displayShipmentId.setText(Integer.toString(selectedShipment.getShipmentId()));
                displayShipmentDateOfReceipt.setText(selectedShipment.getDateOfReceipt());             
            }      
        });
       }
       catch(Exception e) {
           e.printStackTrace();
       }
    }    
    
    private void initializeView() {
        try {
            String query = "select * from Shipment";
            connection = DatabaseAccess.getConnection();
            ResultSet resultSet = connection.createStatement().executeQuery(query);
            retrieveShipments(resultSet);
        } catch (Exception e) {
        }
    }
    
    @FXML
    private void shipmentTestHandler(ActionEvent event) {
        System.out.println("SHIPMENT HANDLER FOUND!!");
    }
    
    private void retrieveShipments(ResultSet resultSet) {
        try {
            saveEdit.setDisable(true);
            listOfShipments.clear();
            while(resultSet.next()) {
                Shipment shipment = new Shipment();
                shipment.setShipmentId(resultSet.getInt(shipment_columns[0]));
                shipment.setDateOfReceipt(resultSet.getString(shipment_columns[1]));                
                listOfShipments.add(shipment);
             }
             shipmentId.setCellValueFactory(new PropertyValueFactory<>("shipmentId"));
             dateOfReceipt.setCellValueFactory(new PropertyValueFactory<>("dateOfReceipt"));            
             shipmentTable.setItems(listOfShipments);
         }
         catch(Exception e) {
            e.printStackTrace();
         }
    }
    
    @FXML
    private void searchShipments(ActionEvent event) {
        try {
            String userQuery = searchBox.getText();
            String search = "";
            if(!userQuery.trim().equals(""))
            {
                System.out.println("not null");
                search = " where shipmentId like '" + userQuery+"%' or dateOfReceipt like '" + userQuery+"%'";
                System.out.println(search);
            }
            String query = "select * from Shipment" + search;
            Connection connection = DatabaseAccess.getConnection();
            ResultSet searchResultSet = connection.createStatement().executeQuery(query);
            retrieveShipments(searchResultSet);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    
       
    @FXML
    private void deleteShipment(ActionEvent event) {
        JFrame frame = new JFrame();
        try {
            if(selectedShipment != null) {    
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.setAlwaysOnTop(true);
                int answer = JOptionPane.showConfirmDialog(frame,
                "Delete shipment entry ?\nItem ID: " + selectedShipment.getShipmentId()+ " Date of receipt: " + selectedShipment.getDateOfReceipt());
                if(answer == 0) {
                    listOfShipments.clear();
                    Connection connection = DatabaseAccess.getConnection();
                    String query = "delete from Shipment where shipmentId = " + selectedShipment.getShipmentId();
                    connection.createStatement().executeUpdate(query);
                    initializeView();
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            frame.dispose();
        }
    }
    
    @FXML
    private void addShipment(ActionEvent event) {
        JFrame frame = new JFrame();
        try {
            
            listOfShipments.clear();

            JTextField jShipmentId = new JTextField(5);
            JTextField jDateOfReceipt = new JTextField(15);
            
            JPanel addShipmentPanel = new JPanel(); 

            //addShipmentPanel.add(new JLabel("Shipment ID: "));
           // addShipmentPanel.add(jShipmentId);

            addShipmentPanel.add(new JLabel("Date of receipt: "));
            addShipmentPanel.add(jDateOfReceipt);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setAlwaysOnTop(true);
            int add = JOptionPane.showConfirmDialog(frame, addShipmentPanel, "New shipment entry", JOptionPane.OK_CANCEL_OPTION);
            if(add == JOptionPane.OK_OPTION){               
                Connection connection = DatabaseAccess.getConnection();
               // int shipmentId = Integer.parseInt(jShipmentId.getText();
                String dateOfReceipt = jDateOfReceipt.getText();                
                
              connection.createStatement().executeUpdate("insert into Shipment(dateOfReceipt) values ('"+dateOfReceipt+"')");
                                                                     

            }
            initializeView();
        
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            frame.dispose();
        }
    }
    
    @FXML
    private void updateShipment(ActionEvent event){
            listOfShipments.clear();
            Connection connect = DatabaseAccess.getConnection();

            try {
                int update = connect.createStatement().executeUpdate("update Shipment set dateOfReceipt = '" 
                        + displayShipmentDateOfReceipt.getText() 
                        + "' where shipmentId = " + selectedShipment.getShipmentId());
            } catch (SQLException ex) {
                Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            initializeView();
    }
    
    @FXML
    private void fieldModified() {
        if(selectedShipment != null) {
            modificationsMade = true;
            saveEdit.setDisable(false);
        }
    }

}
