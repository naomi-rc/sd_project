/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.controller;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import reservationsystem.model.Customer;
import reservationsystem.DatabaseAccess;
import reservationsystem.FXMLDocumentController;

/**
 *
 * @author cstuser
 */
public class CustomerController implements Initializable {
    private String[] customer_columns = {"customerId", "firstName", "lastName", "phoneNumber", "email", "depositAmount"};
    private FXMLDocumentController mainController;
    
    @FXML
    private VBox customersTab;
    @FXML
    private TableView<Customer> customerTable;
    @FXML
    private TableColumn<Customer, String> customerFirstName;
    @FXML
    private TableColumn<Customer, String> customerlastName;
    @FXML
    private TableColumn<Customer, String> customerPhone;
    @FXML
    private TableColumn<Customer, String> customerEmail;
    @FXML
    private TableColumn<Customer, String> customerDeposit;
    @FXML
    private TextArea temp;
    @FXML
    private TextField customerSearch;
//    @FXML
//    private TextField customerName;
    @FXML private TextField firstNameField;
    @FXML private TextField lastNameField;
    @FXML
    private TextArea customerComments;
    @FXML
    private TextField txtCustomerPhone;
    @FXML
    private TextField txtCustomerEmail;
    @FXML
    private TextField customerDepo;
    @FXML Button saveEdit;
    
    ObservableList<Customer> listOfCustomers =  FXCollections.observableArrayList();
    private boolean modificationsMade = false;
    private Customer selectedCustomer;
    private Connection connection;

    public CustomerController() {
        
    }
    
    public void injectMainController(FXMLDocumentController mainController) {
        this.mainController = mainController;
        System.out.println("just received inject");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        connection = DatabaseAccess.getConnection();
        readCustomers();
        customerTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null)
            {
                saveEdit.setDisable(true);
                selectedCustomer = newSelection; 
                System.out.println(selectedCustomer.toString());
                firstNameField.setText(selectedCustomer.getFirstName());
                lastNameField.setText(selectedCustomer.getLastName());
                txtCustomerPhone.setText(selectedCustomer.getPhoneNumber());
                txtCustomerEmail.setText(selectedCustomer.getEmail());
                customerDepo.setText(String.valueOf(selectedCustomer.getDepositAmount()));
            }      
        });
        
    }  
    
    public void readCustomers(){
        try {
            saveEdit.setDisable(true);
            Connection connect = DatabaseAccess.getConnection();
            ResultSet res = connect.createStatement().executeQuery("select * from Customer");
            while(res.next())
            {
                listOfCustomers.add(new Customer(res.getInt(customer_columns[0]), res.getString(customer_columns[1]), 
                        res.getString(customer_columns[2]), res.getString(customer_columns[3]), 
                        res.getString(customer_columns[4]), res.getDouble(customer_columns[5])));
            }
            customerFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
            customerlastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            customerPhone.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
            customerEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
            customerDeposit.setCellValueFactory(new PropertyValueFactory<>("depositAmount"));
        } catch (SQLException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        customerTable.setItems(listOfCustomers);
    }
    
    @FXML
    private void searchCustomer(ActionEvent event) throws SQLException{
        listOfCustomers.clear();
        String search = customerSearch.getText();
        try {
            Connection connect = DatabaseAccess.getConnection();
            ResultSet res = connect.createStatement().executeQuery("select * from Customer where firstName like '"+search+"%'");
            while(res.next())
                {
                    listOfCustomers.add(new Customer(res.getInt(customer_columns[0]), res.getString(customer_columns[1]), 
                            res.getString(customer_columns[2]), res.getString(customer_columns[3]), 
                            res.getString(customer_columns[4]), res.getDouble(customer_columns[5])));
                }
                customerFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
                customerlastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
                customerPhone.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
                customerEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
                customerDeposit.setCellValueFactory(new PropertyValueFactory<>("depositAmount"));
        } catch (SQLException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        customerTable.setItems(listOfCustomers);    
    }
    
    @FXML
    private void deleteCustomer(ActionEvent event) throws SQLException{
         JFrame frame = new JFrame();
         try {
            listOfCustomers.clear();
         
            if(selectedCustomer != null) {               
                frame.setAlwaysOnTop(true);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                int answer = JOptionPane.showConfirmDialog(frame,
                "Delete customer entry?\nCustomer: " +  selectedCustomer.getFullName());
                if(answer == 0) {                 
                    String query = "delete from Customer where customerId = '" + selectedCustomer.getCustomerId()+"'";
                    connection.createStatement().executeUpdate(query);   
                }
                
                readCustomers();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }   
        finally {
          frame.dispose();  
        }
    }
    
    @FXML
    public void updateCustomer(ActionEvent event) {
        listOfCustomers.clear();
        Connection connect = DatabaseAccess.getConnection(); 
        double deposit = 0;
        if(customerDepo.getText().equals(""))
            deposit = 0.0;
        
        else if(customerDepo.getText() != "")
            deposit = Double.parseDouble(customerDepo.getText());
        
        try {
            int update = connect.createStatement().executeUpdate("update Customer set firstName = '" + firstNameField.getText() 
                    + "', lastName = '" + lastNameField.getText() + "', phoneNumber = '" + txtCustomerPhone.getText()
                    + "', email = '" + txtCustomerEmail.getText() + "', depositAmount = "
                    + deposit + " where customerId = "
                    + selectedCustomer.getCustomerId());
        } catch (SQLException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        readCustomers();
    }
    
    @FXML
    public void addCustomer(ActionEvent event) throws SQLException{
        JFrame frame = new JFrame();
        try {
            listOfCustomers.clear();   
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setAlwaysOnTop(true);
            JTextField jFirstName = new JTextField(15);
            JTextField jLastName = new JTextField(15);
            JTextField jPhoneNumber = new JTextField(15);
            JTextField jEmail = new JTextField(15);
            JTextField jDepositAmount = new JTextField(15);

            JPanel addCustomerPanel = new JPanel(new GridLayout(0,2)); 

            addCustomerPanel.add(new JLabel("First Name: "));
            addCustomerPanel.add(jFirstName);

            addCustomerPanel.add(new JLabel("Last Name: "));
            addCustomerPanel.add(jLastName);

            addCustomerPanel.add(new JLabel("Phone: "));
            addCustomerPanel.add(jPhoneNumber);

            addCustomerPanel.add(new JLabel("Email: "));
            addCustomerPanel.add(jEmail);

            addCustomerPanel.add(new JLabel("Amount Deposited: "));
            addCustomerPanel.add(jDepositAmount);

            int add = JOptionPane.showConfirmDialog(frame, addCustomerPanel, "New Customer", JOptionPane.OK_CANCEL_OPTION);
            if(add == JOptionPane.OK_OPTION){
                System.out.println("Pressed Ok");
                Connection connect = DatabaseAccess.getConnection();
                String firstName = jFirstName.getText();
                String lastName = jLastName.getText();
                String phoneNumber = jPhoneNumber.getText();
                String email = jEmail.getText();
                double deposit = Double.parseDouble(jDepositAmount.getText());

                int insert = connect.createStatement().executeUpdate("insert into Customer(firstName, lastName, phoneNumber, email, depositAmount) values ('"
                                                                     +firstName+"', '"+lastName+"', '"+phoneNumber+"', '"+email+"', '"+deposit+"')");

            }
            readCustomers();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            frame.dispose();
        }
        
    }
    
    @FXML
    private void fieldModified() {
        if(selectedCustomer != null) {
            modificationsMade = true;
            saveEdit.setDisable(false);
        }
    }
    
}
