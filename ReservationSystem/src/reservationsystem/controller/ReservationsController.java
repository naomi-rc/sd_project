/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.controller;

import java.text.*;
import java.util.Date;
import java.awt.GridLayout;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.*;
import reservationsystem.model.Columns;
import reservationsystem.model.Customer;
import reservationsystem.DatabaseAccess;
import reservationsystem.model.Employee;
import reservationsystem.model.InventoryItem;
import reservationsystem.model.Reservation;


/**
 *
 * by : Derek Murphy with help from Harry M
 */
public class ReservationsController implements Initializable{
    private DatabaseAccess databaseAccess;
    private ObservableList<String> tableList;
    private Reservation selectedReservation;
    private ResultSet res;
    private boolean modificationsMade = false;
    
    @FXML private Button newReservation;
    @FXML private Button reservationSearch;
    @FXML private Button delete;
    
    @FXML private TableView<Reservation> reservationTable;
    @FXML private TableColumn<Reservation,String> customer;
    @FXML private TableColumn<Reservation,String> product;
    @FXML private TableColumn<Reservation,String> itemType;    
    @FXML private TableColumn<Reservation,Double> itemPrice;
    
    @FXML private ComboBox customerNameField;
    @FXML private ComboBox itemField;//
    @FXML private TextField typeField;
    @FXML private TextField priceField;
    @FXML private ComboBox reservedByField;//
    @FXML private TextField lastUpdatedField;
    @FXML Button saveEdit;
    
//    private ArrayList<String> comboBoxCustomerNames;
//    private ArrayList<String> comboBoxItemsList;
//    private ArrayList<String> comboBoxReservedByList;
    private ArrayList<Customer> comboBoxCustomerNames;
    private ArrayList<InventoryItem> comboBoxItemsList;
    private ArrayList<Employee> comboBoxReservedByList;
//    @FXML 
//    private TableColumn<Reservation,Integer> reservationNumber;
    @FXML TextField searchIt;     
    
    private String[] customer_columns = {"customerId", "firstName", "lastName", "primaryContact", "secondaryContact", "depositAmount"};

    private String[] reservation_columns = {"reservationNumber","productName","firstName","type","price","depositAmount"};
    private String[] inventory_columns = {"productId", "productName", "type", "source", "price", "dateOfRelease", "publisherName"};
    
    
    private ObservableList<Reservation> reservationList= FXCollections.observableArrayList();
    private Connection connection;
    private Reservation oldSelectedReservation;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        connection = DatabaseAccess.getConnection();
        comboBoxCustomerNames = new ArrayList<Customer>();
        comboBoxItemsList = new ArrayList<InventoryItem>();
        comboBoxReservedByList = new ArrayList<Employee>();
        
       /* ChangeListener changeListener = new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!newValue.equals(oldValue) && selectedReservation.equals(oldSelectedReservation))
                    fieldModified();
            }
        };
        customerNameField.valueProperty().addListener(changeListener);
        itemField.valueProperty().addListener(changeListener);
        reservedByField.valueProperty().addListener(changeListener);*/
      
        intializeView();
        reservationTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null)
            {
                saveEdit.setDisable(true);
                selectedReservation = newSelection;
                customerNameField.setValue(selectedReservation.getCustomerName());              
                itemField.setValue(selectedReservation.getProductName());
                typeField.setText(selectedReservation.getReservedItem().getType());
                priceField.setText(Double.toString(selectedReservation.getReservedItem().getPrice()));           
                reservedByField.setValue(selectedReservation.getReservedBy());
                lastUpdatedField.setText(selectedReservation.getLastUpdated());
                        
               // customerNameField.
                /*customerName.setText(selectedCustomer.getFirstName() + " " + selectedCustomer.getLastName());
                txtCustomerPhone.setText(selectedCustomer.getPhoneNumber());
                txtCustomerEmail.setText(selectedCustomer.getEmail());
                customerDepo.setText(String.valueOf(selectedCustomer.getDepositAmount()));*/
            }      
        });
    }   
    
    private void intializeView() {
        try {            
            res = connection.createStatement().executeQuery("select i.productId, c.customerId, c.firstName, c.lastName, r.reservedBy, r.lastUpdated, i.productName, i.type, i.price, c.depositAmount " +
                                "from Customer c, Inventory i, Reservation r " +
                                 "where c.customerId = r.customerId and i.productId = r.productId");
            readReservations();
        } catch (Exception e) {
            e.printStackTrace();
        }            
    }
    
    public void readReservations(){
        try {      
            comboBoxCustomerNames.clear();
            comboBoxItemsList.clear();
            comboBoxReservedByList.clear();
            reservationList.clear(); 
           
            saveEdit.setDisable(true);
                 
            while(res.next())
            {                   
                Reservation reservation = new Reservation();
                Customer customer = new Customer();
                InventoryItem item = new InventoryItem();
                Employee employee = new Employee();
               
                customer.setCustomerId(res.getInt(Columns.RESERVATION_CUSTOMER_ID));
                customer.setFirstName(res.getString(Columns.CUSTOMER_FIRST_NAME));
                customer.setLastName(res.getString(Columns.CUSTOMER_LAST_NAME));
                customer.setDepositAmount(res.getDouble(Columns.CUSTOMER_DEPOSIT_AMOUNT));
                
                item.setProductId(res.getInt(Columns.RESERVATION_PRODUCT_ID));
                
       
               
                item.setProductName(res.getString(Columns.INVENTORY_PRODUCT_NAME));
                item.setPrice(res.getDouble(Columns.INVENTORY_PRICE));
                item.setType(res.getString(Columns.INVENTORY_TYPE));
                
           /*
                reservation.setCustomerId(res.getInt(Columns.RESERVATION_CUSTOMER_ID));
                reservation.setDepositAmount(res.getDouble(Columns.CUSTOMER_DEPOSIT_AMOUNT));
                reservation.setReservedBy(res.getString(Columns.RESERVATION_RESERVED_BY));
                reservation.setItemType(res.getString(Columns.INVENTORY_TYPE));
                reservation.setCustomerName(res.getString(Columns.CUSTOMER_FIRST_NAME) + " " + res.getString(Columns.CUSTOMER_LAST_NAME));              
            */ 
                
                
                employee.setFirstName(res.getString(Columns.RESERVATION_RESERVED_BY));
                reservation.setLastUpdated(res.getString(Columns.RESERVATION_LAST_UPDATED));
                reservation.setCustomer(customer);
                reservation.setReservedItem(item);
                reservation.setEmployee(employee);
                
                reservationList.add(reservation);
           
            }
            //reservationNumber.setCellValueFactory(new PropertyValueFactory<>("reservationNumber")); 
            res = connection.createStatement().executeQuery("select customerId, firstName, lastName from Customer");
            while(res.next()) {
                Customer customer = new Customer();
                customer.setCustomerId(res.getInt(Columns.CUSTOMER_ID));
                customer.setFirstName(res.getString(Columns.CUSTOMER_FIRST_NAME));
                customer.setLastName(res.getString(Columns.CUSTOMER_LAST_NAME));
                comboBoxCustomerNames.add(customer);
            }
            
            res = connection.createStatement().executeQuery("select productId, productName from Inventory");
            while(res.next()) {
                InventoryItem item = new InventoryItem();
                item.setProductId(res.getInt(Columns.INVENTORY_ID));
                item.setProductName(res.getString(Columns.INVENTORY_PRODUCT_NAME));
                comboBoxItemsList.add(item);
            }
            
            res = connection.createStatement().executeQuery("select firstName from Employee");
            while(res.next()) {
                Employee employee = new Employee();
                employee.setFirstName(res.getString(Columns.EMPLOYEE_FIRST_NAME));                
                comboBoxReservedByList.add(employee);
            }
            
            ObservableList<Customer> namesList = FXCollections.observableArrayList(comboBoxCustomerNames);
            ObservableList<InventoryItem> items = FXCollections.observableArrayList(comboBoxItemsList);
            ObservableList<Employee> reservedBy = FXCollections.observableArrayList(comboBoxReservedByList);
            customerNameField.setItems(namesList);     
            itemField.setItems(items);
            reservedByField.setItems(reservedBy);
            customer.setCellValueFactory(new PropertyValueFactory<>("customerName"));
            product.setCellValueFactory(new PropertyValueFactory<>("productName"));
            itemType.setCellValueFactory(new PropertyValueFactory<>("itemType"));
            itemPrice.setCellValueFactory(new PropertyValueFactory<>("itemPrice"));      
        }
        catch (Exception e){
          e.printStackTrace();
       }
        reservationTable.setItems(reservationList);  
    }
    
    @FXML
    private void reservationTestHandler(ActionEvent event) {
        System.out.println("RESERVATION HANDLER FOUND!!");
    }
    
    @FXML
    private void searchReservations(ActionEvent event) {       
        String customer_name = searchIt.getText();
        //JOptionPane.showMessageDialog(null,"Inside ser\n" + customer_name);
                       try {       
            Connection connect = DatabaseAccess.getConnection();
            res = connect.createStatement().executeQuery("select i.productId, c.customerId, c.firstName, c.lastName, i.productName, i.type, i.price, c.depositAmount, r.reservedBy, r.lastUpdated " +
                            "from Customer c, Inventory i, Reservation r " +
                             "where c.firstName like '" + customer_name+"%' and i.productId = r.productId and r.customerId = c.customerId");
            readReservations();
         

      
        }
        catch (Exception e){
           e.printStackTrace();
       }

        reservationTable.setItems(reservationList);  
        // 
        // Select productName from Inventory where productId in (Select productId from Reservation where customerId = (select customerId from Customer where firstname="Andrea" AND lastName="Statton"));
        //       
    }// end of searchReservation
    
    @FXML
    private void removeReservation() {
         JFrame frame = new JFrame();
        try {  
            if(selectedReservation != null) {               
                frame.setAlwaysOnTop(true);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                int answer = JOptionPane.showConfirmDialog(frame,
                "Delete reservation entry?\nCustomer: " + selectedReservation.getCustomer().getFullName()+ "\nItem: " + selectedReservation.getReservedItem().getProductName());
                if(answer == 0) {                 
                    String query = "delete from Reservation where customerId == " + selectedReservation.getCustomer().getCustomerId()+" AND productId == " + selectedReservation.getReservedItem().getProductId();
                    connection.createStatement().executeUpdate(query);           
                    intializeView();
                }                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            frame.dispose();
        }
        
    }
    
    @FXML
    private void newReservation(ActionEvent event) {
         JFrame frame = new JFrame();
        try {   
            ArrayList<InventoryItem> productList = new ArrayList<InventoryItem>();
            ResultSet productResultSet = connection.createStatement().executeQuery("select productId, productName from Inventory");
            while(productResultSet.next()) {               
                InventoryItem item = new InventoryItem();
                item.setProductId(productResultSet.getInt(Columns.INVENTORY_ID));
                item.setProductName(productResultSet.getString(Columns.INVENTORY_PRODUCT_NAME));
                productList.add(item);
            }
            
            ArrayList<Customer> customerList = new ArrayList<Customer>();
            ResultSet customerResultSet = connection.createStatement().executeQuery("select customerId, firstName, lastName from Customer");
            while(customerResultSet.next()) {
                Customer customer = new Customer();
                customer.setCustomerId(customerResultSet.getInt(Columns.CUSTOMER_ID));
                customer.setFirstName(customerResultSet.getString(Columns.CUSTOMER_FIRST_NAME));
                customer.setLastName(customerResultSet.getString(Columns.CUSTOMER_LAST_NAME));
                customerList.add(customer);
            }
        
            ArrayList<String> employeeList = new ArrayList<String>();
            ResultSet employeeResultSet = connection.createStatement().executeQuery("select firstName from Employee");
            while(employeeResultSet.next()) {
                   employeeList.add(employeeResultSet.getString(Columns.EMPLOYEE_FIRST_NAME));
            }
            
            JLabel customerLabel = new JLabel("Customer: ");
            JComboBox customerBox = new JComboBox(customerList.toArray());
            
            JLabel productLabel = new JLabel("Item: ");
            JComboBox productBox = new JComboBox(productList.toArray());
            JLabel dateReservedLabel = new JLabel("Date reserved: ");
            
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            JTextField dateField = new JTextField(dateFormat.format(date));
            
            JLabel employeeLabel = new JLabel("Reservation taken by: ");
            JComboBox employeeBox = new JComboBox(employeeList.toArray());
           
            
            JPanel panel = new JPanel(new GridLayout(0, 1));
            panel.add(customerLabel);
            panel.add(customerBox);
            panel.add(productLabel);
            panel.add(productBox);
            panel.add(dateReservedLabel);
            panel.add(dateField);           
            panel.add(employeeLabel);
            panel.add(employeeBox);            
          
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setAlwaysOnTop(true);
            int answer = JOptionPane.showConfirmDialog(frame, panel, "New Reservation", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (answer == JOptionPane.OK_OPTION) {
                   // insert into Reservation(productId, customerId, lastUpdated, reservedBy)
                //values(1,2,"05/05/2018", "Sylvain");
                int productId = productList.get(productBox.getSelectedIndex()).getProductId();
                int customerId = customerList.get(customerBox.getSelectedIndex()).getCustomerId();
                String lastUpdated = dateField.getText();
                String reservedBy = employeeBox.getSelectedItem().toString();
                String query = "insert into Reservation(productId, customerId, lastUpdated, reservedBy) values(" + productId + ", " + customerId + ", '" + lastUpdated + "', '" + reservedBy + "')";
               System.err.println(query);
               connection.createStatement().executeUpdate(query);
               intializeView();
            }       
            
            frame.dispose();
        } catch (Exception e) { 
            System.err.println("error with add reservation");
            e.printStackTrace();
        }
        finally{
            frame.dispose();
        }
        
    }
    
    @FXML 
    private void updateReservation() {
        try {           
            if(entriesValid()) {                
                //update Reservation set customerId = 2, productId = 5, lastUpdated = "yesterday", reservedBy = "her"
                //where customerId = 1 AND productId = 1;                
                saveEdit.setDisable(true);
                int customerId = selectedReservation.getCustomerId();
                int productId = selectedReservation.getProductId();               
                
                int customerIndex = customerNameField.getSelectionModel().getSelectedIndex();
                int productIndex = itemField.getSelectionModel().getSelectedIndex();
                if(comboBoxCustomerNames.size() != 0 && customerIndex != -1)                    
                {    
                    Customer customer = comboBoxCustomerNames.get(customerIndex);
                    customerId = customer.getCustomerId();
                    System.err.println("YES cus: " + customerId);                    
                }
                if(comboBoxItemsList.size() != 0 && productIndex != -1)                    
                {    
                    InventoryItem item = comboBoxItemsList.get(productIndex);                    
                    productId = item.getProductId();
                    System.err.println("YES prod: " + productId);                    
                }
             
                String reservedBy = reservedByField.getValue().toString();
                String lastUpdated = lastUpdatedField.getText();
                String query = "update Reservation set customerId = " + customerId 
                        + ", productId = " + productId + ", lastUpdated = '" + lastUpdated 
                        + "', reservedBy = '" + reservedBy + "'"
                        + " where customerId = " + selectedReservation.getCustomerId()
                        + " AND productId = " + selectedReservation.getProductId();
                
                System.err.println("QUERY: " + query);
                connection.createStatement().executeUpdate(query);
                intializeView();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void fieldModified() {      
        if(selectedReservation != null) {
            modificationsMade = true;
            saveEdit.setDisable(false);
        }
    }
    
    private boolean entriesValid() {
        return true;
    }
}
