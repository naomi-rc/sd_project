/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.controller;

import java.awt.print.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.util.ArrayList;

/**
 *
 * @author cstuser
 */
public class PrinterController implements Printable {
   // private String content;
    private int[] pageBreaks;
    private String[] textLines;
    private ArrayList<String> customerInfo;
    
    public PrinterController(ArrayList<String> customerInfo) {
        this.customerInfo = customerInfo;
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        printerJob.setPrintable(this);
        boolean okToPrint = printerJob.printDialog();
        if(okToPrint) {
            try {
                printerJob.print();
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public int print(Graphics graphics, PageFormat format, int page) {
        try {
            Font font = new Font("Serif", Font.PLAIN, 20);
            FontMetrics fontMetrics = graphics.getFontMetrics(font);
            int lineHeight = fontMetrics.getHeight();
            
            if(pageBreaks == null) {
                initializePrintData();
                int linesPerPage = (int)(format.getImageableHeight()/lineHeight);
                int numberOfBreaks = (textLines.length - 1)/linesPerPage;
                pageBreaks = new int[numberOfBreaks];
                for(int index = 0; index < numberOfBreaks; index++){
                    pageBreaks[index] = (index+1)*linesPerPage;
                }
            }
            
            if(page > pageBreaks.length) {
                return NO_SUCH_PAGE;
            }
            
            Graphics2D graphics2D = (Graphics2D)graphics;
            graphics2D.translate(format.getImageableX(), format.getImageableY());
            
            int coordY = 0;
            int start = 0;
            int end = 0;
            if(page == 0) {
                start = 0;
            }
            else start = pageBreaks[page - 1];
            
            if(page == pageBreaks.length) {
                end = textLines.length;
            }
            else end = pageBreaks[page];
            
            for(int index = start; index < end; index++){
                coordY += lineHeight;
                graphics.drawString(textLines[index], 30, coordY+ 3);
            }
            return PAGE_EXISTS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    private void initializePrintData() {
        try {
            if(textLines == null) {
                int lines = 50;
                textLines = new String[lines];
                customerInfo.add(0, "CUSTOMER RESERVATION");
                for(int index = 0; index < customerInfo.size(); index++) {
                    textLines[index] = customerInfo.get(index);
                }
            
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
