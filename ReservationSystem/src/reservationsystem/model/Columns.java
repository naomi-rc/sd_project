/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.model;

/**
 *
 * @author Naomi Catwell
 */
public class Columns {
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String EMPLOYEE_ACCESS_LEVEL = "accessLevel";
    public static final String EMPLOYEE_FIRST_NAME = "firstName";

    public static final String CUSTOMER_ID = "customerId";
    public static final String CUSTOMER_FIRST_NAME = "firstName";
    public static final String CUSTOMER_LAST_NAME = "lastName";
    public static final String CUSTOMER_PHONE = "phoneNumber";
    public static final String CUSTOMER_EMAIL = "email";
    public static final String CUSTOMER_DEPOSIT_AMOUNT = "depositAmount";


    public static final String INVENTORY_ID = "productId";
    public static final String INVENTORY_PRODUCT_NAME = "productName";
    public static final String INVENTORY_TYPE = "type";
    public static final String INVENTORY_SOURCE = "source";
    public static final String INVENTORY_PRICE= "price";
    public static final String INVENTORY_DATE_RELEASE = "dateOfRelease";
    public static final String INVENTORY_PUBLISHER_NAME = "publisherName";


    public static final String SHIPMENT_ID = "shipmentId";
    public static final String SHIPMENT_DATE_OF_RECEIPT = "dateOfReceipt";

    public static final String RESERVATION_PRODUCT_ID = "productId";
    public static final String RESERVATION_CUSTOMER_ID = "customerId";
    public static final String RESERVATION_LAST_UPDATED = "lastUpdated";
    public static final String RESERVATION_RESERVED_BY = "reservedBy";

    
}
