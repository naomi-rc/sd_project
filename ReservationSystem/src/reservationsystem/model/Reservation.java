/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.model;

/**
 *
 * @author cstuser
 */
public class Reservation {
    //private int reservationNumber;
    private String productName;
    private String customerName;
    private String itemType;
    private double itemPrice;
    private double depositAmount;
    private int productId;
    private int customerId;
    private String reservedBy;
    private String lastUpdated;
    private Customer customer;
    private InventoryItem reservedItem;
    private Employee employee;
    
    public Reservation() {
        
    }
    
    public Reservation (String firstName){
        this.customerName = firstName;
    }
    
    public Reservation(Customer customer, InventoryItem item, Employee employee, String lastUpdated) {
       /*this.productId = productId;
       this.customerId = customerId;
        this.productName = productName;
        this.customerName = customerName;
        this.itemType = itemType;
        this.itemPrice = itemPrice;
        this.depositAmount = depositAmount;*/
       this.customer = customer;
       this.reservedItem = item;
       this.employee = employee;
       this.lastUpdated = lastUpdated;
    }

//    public int getReservationNumber() {
//        return reservationNumber;
//    }

//    public void setReservationNumber(int reservationNumber) {
//        this.reservationNumber = reservationNumber;
//    }

    ///here
    public String getCustomerName() {
        return this.customer.getFullName();
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getItemType() {
        return this.itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }
///here

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId = customer.getCustomerId();
        this.customerName = customer.getFullName();
        this.depositAmount = customer.getDepositAmount();
    }

    public InventoryItem getReservedItem() {
        return reservedItem;
    }

    public void setReservedItem(InventoryItem reservedItem) {
        this.reservedItem = reservedItem;
        this.itemPrice = reservedItem.getPrice();
        this.itemType = reservedItem.getType();
        this.productId = reservedItem.getProductId();
        this.productName = reservedItem.getProductName();
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.reservedBy = employee.getFirstName();
    }
    
    
    
    

    
   
    
    
    
    
    
}
