/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.model;

/**
 *
 * @author harry
 */
public class Customer {
    
    private int customerId;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private double depositAmount;
    private String fullName;

    public Customer() {
        
    }

    public Customer(int id, String firstName, String lastName, String phoneNumber, String email, double depositAmount) {
        this.customerId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.depositAmount = depositAmount;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int id) {
        this.customerId = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }


    
    
    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
    
    
    
    
    
}
