/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem.model;

/**
 *
 * @author cstuser
 */
public class Shipment {
    private int shipmentId;
    private String dateOfReceipt;

    public int getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(int shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getDateOfReceipt() {
        return dateOfReceipt;
    }

    public void setDateOfReceipt(String dateOfReceipt) {
        this.dateOfReceipt = dateOfReceipt;
    }

    @Override
    public String toString() {
        return "Shipment{" + "shipmentId=" + shipmentId + ", dateOfReceipt=" + dateOfReceipt + '}';
    }
    
    
    
    
    
}
