/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author cstuser
 */
public class DatabaseAccess {
    private final String CUSTOMER_TABLE = "Customer";
    private final String EMPLOYEE_TABLE = "Employee";
    private final String INVENTORY_TABLE = "Inventory";
    private final String RESERVATION_TABLE = "Reservation";
    private final String SHIPMENT_TABLE = "Shipment";
    private String[] customer_columns = {"customerId", "firstName", "lastName", "primaryContact", "secondaryContact", "depositAmount"};
    private String[] reservation_columns = {"productId", "customerId", "lastUpdated"};
    private String[] inventory_columns = {"productId", "productName", "type", "source", "price", "dateOfRelease", "publisherName"};
    
    private String[] shipment_columns = {"customerId", "firstName", "lastName", "primaryContact", "secondaryContact", "depositAmount"};
    private String[] employee_columns = {"customerId", "firstName", "lastName", "primaryContact", "secondaryContact", "depositAmount"};
    
    //Create connection
    //find library Class.forName("org.sqlite.JDBC")
    //set c to DriverManager.getConnection("jdbc:sqlite:test.db")
    //if above works, successful connection to DB has been made
    //C:\Users\cstuser\Desktop\sd_project
    private static Connection connection = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet result = null;
    
    public DatabaseAccess() {
        try {
            Class.forName("org.sqlite.JDBC");
            //connection = DriverManager.getConnection("jdbc:sqlite:test.db");
            connection = DriverManager.getConnection("jdbc:sqlite:cosmix.db");
            System.out.println("Connection success");

        }
        catch(Exception e) {
        System.out.println("Connection failed");
        }
    }
    
     public static Connection getConnection(){
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:cosmix.db");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return connection;
    }
    
    public ArrayList<String> readCustomers() {
        
       ArrayList<String> list = new ArrayList<String> ();
       try {
        String query = "select * from Customer";
        preparedStatement = connection.prepareStatement(query);
        //preparedStatement.setString(0, query);
       result = preparedStatement.executeQuery(); //result is now like a Cursor
       
       //result.first();
       String name = "";
       
       while(result.next()) { //false if there are no more rows
          // info = "Customer "+ result.getRow()  + ": ";
           name = result.getString(customer_columns[1]) + " ";
           name += result.getString(customer_columns[2]);
           
           //System.out.println(info);
           list.add(name);
       }
     
       
       }
       catch(Exception e) {
           System.out.println(e);
       }
       return list;
       
        
               
    }
    
    /*public ArrayList<Reservation> retrieveReservations() {
        ArrayList<Reservation> reservationList = new ArrayList<Reservation>();
        try {
            String query = "select i.productId, c.firstName, c.lastName, i.productName, i.type, i.price, c.depositAmount\n" +
                            "from Customer c, Inventory i, Reservation r\n" +
                              "where c.customerId = r.customerId and i.productId = r.productId";
            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
           
            while(result.next()) {              
                //System.out.println(result.getString("productName"));
                Reservation reservation = new Reservation();
                reservation.setProductId(result.getInt(Columns.RESERVATION_PRODUCT_ID));
                reservation.setCustomerId(result.getInt(Columns.RESERVATION_CUSTOMER_ID));
                reservation.setCustomerName(result.getString(customer_columns[1]) + " " + result.getString(customer_columns[2]));
                reservation.setProductName(result.getString(inventory_columns[1]));
                reservation.setItemType(result.getString(inventory_columns[2]));
                reservation.setItemPrice(result.getDouble(inventory_columns[4]));
                reservation.setDepositAmount(result.getDouble(customer_columns[5]));
                reservationList.add(reservation);
            }
            
        } 
        catch (Exception e) {
             System.out.println(e);
        }
        return reservationList;
    }*/

}
