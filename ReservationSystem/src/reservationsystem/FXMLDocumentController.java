/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem;

import reservationsystem.model.Customer;
import reservationsystem.model.Columns;
import reservationsystem.controller.CustomerController;
import reservationsystem.controller.PrinterController;
import java.awt.GridLayout;
import java.awt.print.PrinterJob;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javax.swing.*;

/**
 *
 * @author cstuser
 */
public class FXMLDocumentController implements Initializable {
    public Button buttonTest;
    @FXML private Button newCustomer;
    @FXML private TableView<String> customerTable;
    @FXML private TableColumn customerName;
    @FXML private AnchorPane bannerPane;   
    TableView tableView;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    private SimpleStringProperty firstName = new SimpleStringProperty();
    private SimpleStringProperty lastName = new SimpleStringProperty();
    @FXML private ImageView banner;
    @FXML private CustomerController customerController;
    private JComboBox<Customer> customerComboBox;
//    FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
//        loader.setController(new FXMLDocumentController());
//    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {     
        System.out.println("IN MAIN CONTROLLER");
        banner.fitHeightProperty().bind(bannerPane.heightProperty());
    }    
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        //newCustomer.setText("Hello World!");
        DatabaseAccess db = new DatabaseAccess();
        ArrayList<String> list = db.readCustomers();
        String result = "";
        for(int i = 0; i < list.size(); i++)
            result+= list.get(i);
       
    }
    
    @FXML
    private void printInfo(ActionEvent event) {
        JFrame frame = new JFrame();
        try {
           
            
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setAlwaysOnTop(true);
            JPanel panel = new JPanel(new GridLayout(0,1));

            //Customer reservation option
            JRadioButton reservationButton = new JRadioButton("Print customer reservation");
            customerComboBox = new JComboBox<Customer>();
            retrieveCustomerList();
        
            panel.add(reservationButton);
            panel.add(customerComboBox);

            //Data table option
            JRadioButton dataTableButton = new JRadioButton("Print data table");
            JComboBox<String> tableComboBox = new JComboBox<String>();
            tableComboBox.addItem("Customer Table");
            tableComboBox.addItem("Reservations Table");
            tableComboBox.addItem("Inventory");
            tableComboBox.addItem("Shipment");
            tableComboBox.addItem("Employee Table");
            panel.add(dataTableButton);
            panel.add(tableComboBox);

            int option = JOptionPane.showConfirmDialog(frame, panel, "Print", JOptionPane.OK_CANCEL_OPTION);
            if(option == 0) {
                ArrayList<String> list = null;
                boolean okToPrint = false;
                if(reservationButton.isSelected()) {
                    int customerId = 0;
                    int customerIndex = customerComboBox.getSelectedIndex();//getSelectionModel().getSelectedIndex();

                    if(customerComboBox.getItemCount() != 0 && customerIndex != -1)                    
                    {    
                        Customer customer = customerComboBox.getItemAt(customerIndex);
                        customerId = customer.getCustomerId();
                        System.err.println("YES cus: " + customerId);                    
                    }
                    list = retrieveCustomerData(customerId); 
                    okToPrint = true;
                }
                else if(dataTableButton.isSelected()) {
                    JOptionPane.showMessageDialog(frame, "Option unavailable");
                }
                else {
                    JOptionPane.showMessageDialog(frame, "Please select a print option");
                }
                
                if(okToPrint) {
                    PrinterJob printerJob = PrinterJob.getPrinterJob();
                    printerJob.setPrintable(new PrinterController(list));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            frame.dispose();
        }
    }
    
    
    private ArrayList<String> retrieveCustomerData(int customerId) {
        ArrayList<String> customerData = null;
        try {
            customerData = new ArrayList<String>();
        
            Connection connection = DatabaseAccess.getConnection();
            String query = "select c.firstName, c.lastName, c.depositAmount,\n" +
                            "i.productName, i.price, i.type\n" +
                            "\n" +
                            "from Customer c, Inventory i, Reservation r\n" +
                            "where c.customerId = r.customerId AND i.productId = "
                            + "r.productId AND c.customerId = " + customerId;
            ResultSet resultSet = connection.createStatement().executeQuery(query); 
           
            customerData.add("Name: " +resultSet.getString(Columns.CUSTOMER_FIRST_NAME) + " " + resultSet.getString(Columns.CUSTOMER_LAST_NAME));
            customerData.add("Deposit amount $" + resultSet.getString(Columns.CUSTOMER_DEPOSIT_AMOUNT));
            customerData.add("");
            while(resultSet.next()) {                
                customerData.add("Reserved item: " + resultSet.getString(Columns.INVENTORY_PRODUCT_NAME));
                customerData.add("Item price: " + resultSet.getString(Columns.INVENTORY_PRICE));
                customerData.add("Item type: " +resultSet.getString(Columns.INVENTORY_TYPE));
                customerData.add("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customerData;
    }
    
    private void retrieveCustomerList() {       
        try {
            Connection connection = DatabaseAccess.getConnection();
            ResultSet resultSet = connection.createStatement().executeQuery("select customerId, firstName, lastName from Customer");
            while(resultSet.next()) {
                Customer customer = new Customer();
                customer.setCustomerId(resultSet.getInt(Columns.CUSTOMER_ID));
                customer.setFirstName(resultSet.getString(Columns.CUSTOMER_FIRST_NAME));
                customer.setLastName(resultSet.getString(Columns.CUSTOMER_LAST_NAME));
                customerComboBox.addItem(customer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
    }
 
}
