/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservationsystem;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Properties;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author cstuser
 */
public class ReservationSystem extends Application {
    private double width = 1025;
    private double height = 855;
    
    @Override
    public void start(Stage stage) throws Exception {
        try { 
            Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
            Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
            width = dimension.getWidth()/1.34;
            height = dimension.getHeight()/1.2;

            Scene scene = new Scene(root, width, height);
            //scene.getStylesheets().add("/layout_styles.css");
            stage.setScene(scene);
            stage.show();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
       
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            launch(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
